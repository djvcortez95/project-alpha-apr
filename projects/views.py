from django.shortcuts import render
from projects.models import Project


def list_projects(request):
    list = Project.objects.all()
    context = {
        "projects": list,
    }
    return render(request, "projects/list.html", context)
